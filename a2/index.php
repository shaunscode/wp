<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ANZAC DouglasR Raymond Baker: Letters Home</title>
    
    <!-- Keep wireframe.css for debugging, add your css to style.css -->
    <link id='wireframecss' type="text/css" rel="stylesheet" href="../wireframe.css" disabled>
    <link id='stylecss' type="text/css" rel="stylesheet" href="style.css?t=<?= filemtime("style.css"); ?>">

    <link href="https://fonts.googleapis.com/css2?family=Give+You+Glory&family=Sora:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Dawning+of+a+New+Day&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Gloria+Hallelujah&display=swap" rel="stylesheet">
    
    <script src='../wireframe.js'></script>
  </head>

  <body>

    <header>
      <div>ANZAC</div>
      <div>Douglas Raymond Baker</div>
      <h1>Letters <br/>Home</h1></div>
    </header>

    <nav>
      <div>
      <ul>
        <li><a onclick="slideTo('section.home');">Home</a></li>
        <li><a onclick="slideTo('section.letters');">Letters</a></li>
        <li><a onclick="slideTo('section.postcards');">Postcards</a></li>
        <li><a onclick="slideTo('section.places');">Places</a></li>
      </ul>
</div>
    </nav>

    <main>
      
      <section class="bg-image bg-image-1">
      </section>

      <section class="home">
        <h2>Home</h2>
        <article class="container">
          <p>Hello and welcome,</p>
          <p>This year is the centenary of the birth of the ANZAC legend. As such, many people, particularly young people, are looking for ways to connect with people of that period and inparticular, those who created the ANZAC legend.</p>
          <p>This site presents the letters of Douglas Raymond Baker, who came from Gympie, Queensland, Australia. He enlisted in August 1914 and during the years that followed, he wrote letters and post cards to his family at home. In them, he describes much of the goings-on of the life of an ANZAC, his feeling and opinions, and experiences while visiting his relatives in England during leave.</p>
          <p>They start from the beginning of basic training in Brisbane in August 1914 and end in May 1918.</p>
          <p>They are offered here so that others may get an understanding of life as an ANZAC and get a sense of what life on the battlefield was like.</p>
          <p>From the menu on the left, read the Introduction to set the scene. Then, to start reading the letters, click on Letter and Post Cards in the menu on the left. All the letters are searchable using the  search bar at the top right of this page.</p>
        </article>
      </section>

      <section class="bg-image bg-image-2">
      </section>

      <section class="letters">
        <h2>Letters</h2>            
        <div class="container flex-box">
          <div class="menu">
            <ul>
              <li><a onclick="loadContent('section.letters', '.letter-070914');">September 7th. 1914.</a></li>
              <li><a onclick="loadContent('section.letters', '.letter-131014');">October 13th.  1914. </a></li>
              <li><a onclick="loadContent('section.letters', '.letter-111214');">December 11th.  1914</a></li>
            </ul>
          </div>
          <div class="content">
            <div class="letter-070914 visible-content">
              <h3>September 7th. 1914.</h3>
              <article>
                <p>It is Saturday morning and nearly all our Coy. are half dead from the effects of inoculation which took place yesterday afternoon.</p>
                <p>It is absolutely deadly. It is nothing for two or three fellows to drop clean out to it while on parade without any warning whatever. We are just beginning to feel the vaccination and were inoculated on the same arm and have to wear that useful member in a sling. It effects one all over  -  head, back and everywhere else.</p>
                <p>Just after writing home on Thursday night, the parcel of eatables was brought to me. It was just the thing and the chaps in our tent were talking of writing to Mrs. Baker and telling her what they thought of her. Fancy that. We still have some cake and coconut ice left.</p>
                <p>Yesterday was pay-day although it should have been last Monday but the pay sheet was not ready. I started an account in the Commonwealth Savings Bank, there is a branch here in camp, and I have arranged for 2/6 (Two Shillings and Six Pence) a day to be placed to my credit while we are away.</p>
                <p> We do not know when we will leave here but think it will be soon. We will all be allowed to have our vote today.  Breakfast is ready so I will close with love to you all at home.</p>
            </article>
          </div>

            <div class="letter-131014">
              <h3>October 13th. 1914. </h3>
              <article>
              <p>Your letter and Al’s came to hand today and were very welcome.  I was sorry to hear that you had been having a bad time and hope you keep in good health now that you are on the mend.  You were inquiring about the letters from home, well, I think I have received them all so far.  We still remain at Port Melbourne and have no idea when we will be leaving. It will be better for all of us when we do for we get through our money very quickly and most of us are stoney broke. We only get one shilling a day now as an allowance and it does not go far.</p>
              <p>I hardly remember where I left off last time I wrote home and don’t want to tell you the same news twice. We go out marching and drilling every day and see a good deal of the suburbs around Port Melbourne, you know we do not always drill in the same place.  Today we went by train to a place called Heidelberg about ten or twelve miles away. </p><p>We had to change trains at Flinders Street Station and crowds of people gathered round to have a look at us. It is the first time we have been in the city, that is, all together, although of course it only took a few minutes to cross the road to where the other train was waiting.</p>
              <p>The country round Heidelberg is very open, hilly and grassy and well suited to the carrying out of military exercises.  We put in the whole day there and returned to the ship about 5 p.m. feeling just fairly tired out.  Tell Mum I have had my watch mended and it is now working splendidly. Last Saturday we had leave from 2 p.m. till 11 p.m. and another chap and I walked around the shore to St. Kilda  –  quite  4 miles. </p><p>It was grand – a lovely road all the way with nice houses on one side and the glorious sea on the other and here and there small gardens and plots of green grass.  There is no doubt that the Council here are succeeding in making the city very pretty, especially the outskirts.  St. Kilda is a very pretty place and seems to be a favourite spot.  There are numerous places of entertainment and hot and cold sea baths everywhere.  We found plenty to look at and did not notice the time passing.  At six we had tea, [ham and eggs, etc.] then strolled about again for a while but it got too cold after dark so we went into a dance hall, much larger than any in Gympie, well lighted and with a beautiful floor.  They charged threepence admission for everybody and if you wanted to dance you had to pay threepence each time. The dancing space is railed off and only those who buy dancing tickets are allowed on the floor.  They only have two dances, waltz and two-step, turn about at each, no square dances at all.  The chap I was with [Tom Johnston of the Scottish] had a few dances and then we made our way home by tram and train.</p>
              <p>By going to St. Kilda we missed a very exciting time in the city.  Perhaps you may have read something about it in the papers, very likely untrue, for the papers here got hold of it wrongly, although one of them said the Australian troops concerned should be the right sort to send to defend our country.  Are you wondering what I am getting at? </p><p>It is very hard to write here with heated arguments going on all round and I find it hard to collect my thoughts for I cannot help listening to the other chaps.  At times we have very interesting discussions and sit for hours talking on the one subject.  There are some very well educated chaps here  -  three in our Company can speak French and German fluently.  But I am getting away from the subject  -  I was about to tell you of a riot that occurred on Saturday afternoon in which the Queensland troops were the chief actors.  The police were the cause of it  -  at least one policeman in particular was and he ended up in the hospital.   The Melbourne police are very cocky and this one noticed a couple of our chaps standing on the edge of the footpath in one of the main streets talking quietly together and ordered them to move on.  They refused so he pushed them off and they stepped back again, so he grabbed one of them by the neck and threw him into the street.  This soldier lad resented such rough treatment  and, being something of a fighter, he made straight for the John (john hop=cop). There was only two hits in it and the policeman got both  -  a beautiful uppercut on the jaw and another with the pavement on the back of his head.  Then things began to hum – police came from all directions and as there were numerous soldiers about the town they lost no time in getting together.  Of course the civilians lost no time in rolling up either.  The fighting then became general, the police using batons and hand-cuffs and our lads their fists.  The cops got the worst of it and several of them were taken to hospital.  One of our chaps got a nasty smack on the jaw with hand-cuffs and others were bruised a little.  All traffic was stopped and, according to the papers here, the crowd numbered fully 20,000.  There were 200 foot police out and a number of mounted.  After a couple of hours things quietened down, a few civilians being arrested but no soldiers.  This first row started 4.30 p.m. and there were two more after this.  It was not till after midnight that everything had settled down and peace restored.  Our fellows were awfully excited when they got home and we sat up talking until about 2 a.m..  We expected that all leave would be stopped but nothing was said the next morning.  All that Colonel Lee did was to place a picquet of about twenty men in town every night since the row, to see that no more occurred. Sunday morning church parade was held on the wharf and afterwards general leave was granted.  We (the Gympie crowd) waited till after dinner and then went off together, taking the tram to the city and then made for the Zoological Gardens where we had a most enjoyable time. The place is well laid out, trees, gardens and lawns everywhere and a fine collection of animals and birds such as an elephant, zebra, brown and polar bears, lions and tigers, a jaguar, Tasmanian Devil, monkeys, deer of all sorts, bison, hippos, cassowary, emu, ostrich, eagles and many others. We had tea in town and finished up at a picture show, getting back to the “Omrah” about 11.15 p.m..</p>
              <p>Best love to all.</p>
            </article>
            </div>

            <div class="letter-111214">
              
              <h3>December 11th.  1914 </h3>
                <article>
              <p>At last I am able to write a few lines home, although I will have to borrow a stamp as I am, like most of us  - stoney broke.  We have now settled down to camp life again but do not know how long we will be here or how soon we will be in the firing line.  It was a great surprise to us when we learned we were to come to Egypt.  I will try to tell you all the news since last I wrote so will start from where I left off which was when we were at Aden. We dropped anchor at that port on the morning of Nov. 25th with most of the other ships  and two cruisers [a Japanese and the Melbourne] it is not a very large place and is very barren looking, not a sign of grass or trees anywhere.  There are ranges of mountains in all directions - great sharp looking peaks sticking up everywhere.  At Aden we took in coal and water.  There were fully 200 niggers on the job coaling from ten barges of coal.  These niggers are like a swarm of ants and keep up a noisy babble all the time they work. Sometimes they would stop work to pray to “Allah” by kneeling down on a bag and bowing their heads down to the ground two or three times, then stand up with arms folded and head bowed for a minute and then down on their knees again.  They would repeat this four or five times.  Many of the Arabs came round the ship in boats selling cigarettes, postcards, etc. and nearly always started prices at four shillings (40c.) but came down to sixpence (5c.) at the finish.</p>
              <p>On Thursday,  8 a.m., Nov. 26th we left Aden, steaming at about 11 knots and the Arabian coast was in sight all day and the African coast towards night.   We passed a few Arab villages where there were many camels and at one place we saw the hulk of a ship that had been wrecked.  That night we had tea at 5.15 and all lights were put out at 6 o’clock as there was supposed to be a hostile fort about somewhere, but nothing occurred.  The next day we were in the Red Sea and by Sunday, Nov. 29th, we had had enough of it for the heat was terrific and the sun would set like a ball of fire.  There was nothing of interest in the Red Sea except a passing vessel now and again, mostly Indian troopships returning to India.</p>
              <p>It was on Saturday, Nov., 28th. that we first received word that we were to be landed in Egypt and on the Sunday following, Colonel Merrington preached his last sermon as he was going with the Light Horse when we landed.  He preached a very interesting sermon and a very appropriate one, as we were in the Red Sea at the time. It was about what happened 25,000 years ago [so he said] when the Israelites crossed the Red Sea.</p>
              <p>We entered the Gulf of Suez on Monday Nov. 30th and on that day an inspection of our kits was held to see if we had lost anything [I had not].  We were also paid thirteen shillings each which made altogether Three Pounds and One Shilling paid to us while on board since September 24th to Dec. 7th. when we disembarked at Alexandria.  Next week we are to be paid all the back money we had arranged for before leaving Enoggera.  I had signed to draw two shillings and sixpence per day and to have the rest banked for me or sent home. So next week I will receive about Six Pounds Six Shillings and sixpence.  After that we are to get two shillings per day as long as we are in Egypt, paid every week and the balance, three shillings per day will be credited to us .  Now I will resume my tale.</p>
              <p>On Tuesday, Dec 1st we anchored just outside the town of Suez and shortly afterwards the H.M.S.Hampshire came alongside and took off the German prisoners, who seemed sorry to leave us. They thanked the guard for the kind treatment they had received and as they went away, waved and sang out, “Goodbye” many times. I think they were quite satisfied to be prisoners and had had enough of war. I have a Mexican dollar that I got from one of them, it is about the size of a five shilling piece.</p>
              <p>There were many boats outside Suez and niggers came to the side selling turkish delight, dates, figs, etc..  As much as we could see of the town looked very pretty – many fine looking buildings and trees about.  At 10 o’clock the same night we entered the canal, having a searchlight shining in front, which I think is compulsory when ships pass through in the night.  It was a bright moonlight night and we could see things fairly well on shore although we were wishing it had been daylight.  The town of Suez extends right to the edge of the canal and looked great, lit up by electricity.  We passed many dredges and other craft, also saw a railway station and small villages now and again on shore.  The railway line runs parallel with the canal and is on the African side, of course. We moved very slowly and when we got up next morning still had a long way to go.  It was very interesting and there was always something to look at.  The country on either side of the canal is very dreary looking; part of it has been flooded to keep back the Turks.  There are many military camps on each bank, one in particular being very large, composed mostly of Indian soldiers.  They had breastworks thrown up and barbed wire entanglements  all round and seemed quite ready for an attack.  They cheered us as we passed, everywhere we get a good reception.  The railway stations are all very pretty  -  built of stone with a tiled roof and gardens round them.  We noticed an armoured train at one and many soldiers about.  A few miles from Port Said we passed a French passenger steamer and the people on board made a great fuss as we passed by. At 11.30 a.m. Wednesday December 2nd  we steamed into Port Said which was completely crammed with shipping and only a narrow passage left for us to pass through.   We passed in quick succession, two British Cruisers and three French cruisers.  The Frenchmen cheered us madly and when our band struck up their National Anthem they gave three “Hip, Hip, Hoorahs”  If sounded dead funny, the way they “Hip” was like “Heep”.  There were all sorts of native boats, some of them have enormous sails, you would think they would be blown over but the niggers manage them perfectly.  As soon as we anchored the bum-boats, as they are called, were round us, as in other places, selling their wares. They were much cheaper here than at Suez, Aden or Colombo.  We coaled up here and the niggers were very quick, I believe Port Said holds the record of the world for coaling. </p>
              <p>Port Said seems to be a very large place, there are many imposing looking buildings, but we could not see much from the boat and were not allowed ashore. At the entrance of the Canal is a statue of “De Lesseps”  [bronze, I think]  in a standing position, a roll of paper [plan of the canal, no doubt] in his left hand and his right hand flung out in the direction of the canal, just as if he were inviting the whole world to enter and admire his work.  I thought it very good.  At this port there were many German boats stranded and the coal was being taken from them as we passed.  Ships were coming and going all the time and the niggers swarm all over the place in all sorts of boats.</p>
              <p>Tuesday morning, Dec. 3rd.  We moved out from Port Said about a mile and dropped anchor to make room for other boats. Half a dozen other troopships went with us.  About midnight that same night we started for Alexandria, at which port we arrived on the 5th Dec..  There is a large breakwater at this port and accommodation for hundreds of ships.  The sight of the shipping was great and a bit confusing, whichever way you looked you would see wharfs and ships, motor boats, steam launches and niggers.  The city itself seemed to be very large but we saw nothing of it except when passing through in the train on our way to Cairo.  The work of unloading the “Omrah” took some time and our Coy. were on the job as well as a crowd of dirty niggers.  I call them niggers for want of a better term.  They are all shades from jet black to nearly white and consist of Arabs, African blacks, Egyptians and other mixtures.  Anyway they were a lazy lot of beggars but we made them work.  While we were loading the trucks we were pestered nearly out of our lives by other niggers selling oranges, cigarettes and chocolates.  But we had no money.  Some well dressed Egyptians were on the game of exchanging their money for English and if anyone from the ship exchanged with them they found out after that they had been done in.  Their money is very confusing but I suppose we will get used to it, we will have to as very little English money is in circulation.  Many of the niggers refuse Australian money saying it is no good.  We entrained at 12 o’clock noon, Monday Dec. 7th at Alexandria and the journey to Cairo was very interesting, the most notable thing being the ancient look about everything – there being many old ruins and tumble down walls. The train travelled fairly fast, we had third class coaches – could walk from one end of the train to the other but it was very dirty.  It was driven by Egyptians and at all the stations the staff were Egyptians.  The country we passed through is very flat and great farming land, being well irrigated from the river Nile.  There are no fences anywhere between the farms or along the line, nothing to tell where one farm ends and another begins. For miles in places there are no houses and those we did pass were all built entirely of mud.  Much cotton is grown here and we passed a large mill where there were thousands of bales of cotton stacked. Dozens of camels were being used for carrying cotton, sometimes we saw as many as twenty in one long line.  We saw many villages and now and again passed a large town. It is said that the cotton trade of Egypt will suffer greatly through the war.  Everywhere there are thousands of niggers in their sloppy looking dress, we get absolutely sick of them.  There are a lot of dead ones, too, for we saw many of the strange grave-yards, sometimes right in the middle of the town. </p>
              <p>We arrived at Cairo at 4.30 p.m. and removed ourselves and our baggage from the train and found that the military people had hot cocoa and bread and cheese.  Each man filed past and received his ration, it was bosker.  A horde of niggers removed our baggage on to wagons drawn by mules and donkeys and it was taken a few hundred yards and loaded on to trams.  It was then run out by the trams, which are electric, to within a mile of the camp where another mob of niggers with wagons carted it to the camping ground.  We had nearly an hours wait before enough trams were ready to take us and we spent the time singing and trying to talk to the natives. It was pitch dark when we started so we could not see what Cairo was like.  I know we crossed the Nile twice and one bridge we crossed was an awful length. I hope to make the trip in daylight sometime. </p>
              <p>We are nine miles from Cairo, we were jolly glad we did not have to march.  The first thing we saw when we got out of the trams was the two large pyramids looming up out of the darkness, they looked big against the sky. After leaving the train we had a mile to march over sand, had our tea in the dark, sorted our our baggage and went to bed in the open.  We had two blankets issued to each  but there were no tents and the night was very cold.  The following morning two others and myself had to report for police duty at six o’clock to the Brigade Major who gave us the job of walking about and keeping order and seeing that no natives went through the lines.  We are within half a mile or so of the Pyramids with a large sandy hill between us so that from the camp we can only see two of the Pyramids from halfway up to the top.  For three nights we were without tents and the second and third nights it rained and we were cold, wet and miserable.  Now we have one tent for each section but we expect more at any time.  Of course things are not properly settled yet, there must be from 15,000 to 20,000 men here, so they will take a little looking after.</p>
              <p>Besides twelve Battalions of Infantry of 120 each, there is Artillery, Machine gun sections, Army Service Corps, Field Ambulance, and Army Medical Corps and others.   We do not get quite enough to eat, for instance, yesterday we had for the day, 1 loaf of bread for five men, 3 navy biscuits, ½ pound of meat, and one small tin of sardines for two men and tea for two meals.  One great curse is the sand, it gets everywhere, in our food, our rifles, our clothes, our boots, in fact we eat it, drink it, wash in it, work in it and sleep in it, there is absolutely no grass or trees anywhere.  But we are getting used to it fast and feel on the whole quite satisfied.  The air is very bracing but rather cold at night, the winter months here are Dec., Jan., and Feb..  I do not know how we would have stood the cold in England had we gone there.  Many fellows go to town at night but I have not yet been, having no money.  They always go in groups for some of the niggers are dangerous.  A few nights ago one of the New Zealand fellows had his throat cut by some Mahomedans and died soon afterwards.  It served him right in a way for he was drunk and tore the veil from one of the women’s faces and that is a great insult. The night after, a Sergeant in the next Coy. to ours was run over by a motor car and had both legs broken and got concussion of the brain, he is in a serious condition.  The day after that a chap in the 1st Battalion was climbing the big pyramid and had got halfway up when he lost his balance and fell.  Needless to say, he was completely settled and died the same night.</p>
              <p>Our Company had a morning off the other day and eleven of us took a walk over the sandhill to see the pryamids.  Before we got half way we were being pestered by dozens of guides who wanted to show us round but we had no money.  That did not trouble them much, they said if we would pay them when we were paid it would do. They were quite satisfied if we would promise for they say an Englishman never breaks his word.  So we engaged one who charged us sixpence a head [to be paid next Wed.] and we spent about 2 hours in his company.  He was an Egyptian and could speak English and French and he explained many things to us.  We first visited the large pyramid [Cheops, I think] and walked round the bottom.  It is indeed wonderful, you would wonder how such great stones were put in place and to such a height.  Many of the large blocks of stone are crumbled and broken with ages of wear.  The guide explained a lot about it but I cannot remember much.  We did not enter the inside as an extra charge is made but will do so another time.  The second pyramid is quite close to the first and the third a little farther away. The top of the second one is quite smooth looking and I think they must have all been like that at one time.  We next visited a sacred well about 50 feet (15 metres) deep and square and in the sides were openings where coffins had been.  One stone coffin was still there and the inside of it was cut out the shape of the body.  Fifty yards (45 metres) from this was the Sphinx which showed very much the wear and tear of ages, but still it is a great bit of work.  The nose is completely gone and other parts are gone.  The sand is banked up against it and hides the body.  The guide told us that it had the body of a lion, the head of a man and the face of a woman.   He said scientists had excavated round it many times to try and find an opening to the inside but never could.  There is a sacred temple nearby which the guide said was the temple of the second pyramid because a passage led from the temple straight to the pyramid.  This temple has been a marvellous piece of work but like everything else shows signs of great age.  There are huge polished granite columns and cross pieces on top fitted together as neat as could be.  It is almost buried in sand to the top of the walls and one can stand and look down into it for there is very little roof on it.  The entrance to it is by a narrow passage with concrete walls on either side to keep back the sand.  Making a tour round and back towards the big pyramid we entered an almost buried temple and there were some great carvings, just like you see in some bible pictures.  There were figures of men, women and animals and also writings carved out of the solid stone.  The guide said the figures of the people carved there were supposed to represent those who had been buried in the temple.  We also entered another place similar to this.  It was very interesting and we intend to make another trip to get inside the large pyramid and also climb to the top outside.</p>
              <p>Well, dear home folks, I believe I have told you all the news in a sort of way and it has taken me two days to write this.  We do not get much time to ourselves and things are rather inconvenient, it gets dark at 5 p.m. and we have no light and there is no Y.M.C.A. here for us.  I will not be able to write all this over again, you can let all friends know the news.  So far we have been very lucky and have had a good time.  Our voyage out on the “Omrah” was very smooth and a great experience for us.  One thing we could not help seeing and that is that every port of any importance is held by the British, each one being of great value from a national point of view.</p>
              <p>You all have my best love  and I hope some day to see you all again.  We do not know how long we will be here, for there are all sorts of rumours.  Some say six months and some say a few weeks.  We may go to Palestine where there is supposed to be 80,000 Turks.  Anyway, I will write as often as I can and let you know how I am progressing.   Remember me to all Gympie friends.   All the Gympie boys here are well.</p>
            </article>
            </div>
          </div>
        </div>
      </section>
      
      <section class="bg-image bg-image-3">
      </section>

      <section class="postcards">
        <h2>Postcards</h2>            
        <div class="container flex-box">
          <div class="menu">
            <ul>
              <li><a onclick="loadContent('section.postcards', '.postcard-3');">Post Card. August 25th 1914.</a></li>
              <li><a onclick="loadContent('section.postcards', '.postcard-1');">August 31st. 1914</a></li>
              <li><a onclick="loadContent('section.postcards', '.postcard-2');">Post Card to Dad, written in trenches, 21/7/15</a></li>
            </ul>
          </div>
          <div class="content">

            <div class="postcard-1 visible-content">              
              <h3>August 31st. 1914</h3>          
              <div class="postcard-back">
                <p>On Saturday when we came into camp from drill at 12 o’clock, we were told that the Commandant was very pleased with the progress made and had granted us a half holiday so the Gympie lads decided to go to the city. We had a good time in Brisbane and had tea at a fish shop.  Met Herb at about 7 p.m. and we all went to the Empire but the show was very poor. We have had issued to us two towels, two pairs under-pants, two singlets, two pairs socks, a sweater and a cake of soap. I think we will get boots in a day or two and other things.</p>
                <p>A church parade was held but none of the Gympie lads attended. It happened to be a voluntary one! We are all keeping in pretty good health and hope you all are at home. Today is payday, we are to get paid every fortnight, this time we will get 11 days. I am only drawing half of mine and banking the rest. Love to all.</p>
              </div>
            </div>

            <div class="postcard-2">              
              <h3>Post Card to Dad, written in trenches, 21/7/15</h3>
              <div class="postcard-back">
                <p>Still going strong and keeping in good health.  I was awfully pleased to get Mother’s letter but the parcel has not yet arrived, nor has Dot’s letter.  Always address my letters to Egypt as I think I will have a better chance of getting them.  Many Australian papers find their way here, I have seen those up to June 5th – some Gympie ones among them.  Any news from Australia is welcome here, Gympie news to me especially.  My news must keep, for reasons known to you.  Hope all at home are well.  Best of love to all.</p>
              </div>
            </div>

            <div class="postcard-3">              
              <h3>Post Card. August 25th 1914.</h3>
              <div class="postcard-back">
                <p>Just have a chance to write a few lines at a small shop near the Camp. We had about four hours in Brisbane before going to Camp and had a good look round. Arrived in Camp about 6 p.m. and all the Gympie lads [Infantry] were placed in one tent [11 of us].  The Light Horse are nearly a mile from us. Of course it was my luck to be made Tent Orderly for the first day – getting the tucker and cleaning up. We have had nothing issued to us yet, so just as well I brought something.  There is no child’s play in this camp – plenty of hard work – they mean business. We have a free hand at night so far and can write as we like.  Saw Herb as we were marching to the Camp. He was going back from work and I had only time to shake hands with him – could not leave the ranks. We will be under Captain Jackson. He has been appointed Captain of the Northern Rivers men and managed to get us in with him. Cannot write any more, am in a hurry. Will write more later on. Am feeling quite homesick. Had our feet examined today, teeth tomorrow I think.</p>
              </div>
            </div>

          </div>
        </div>

      </section>

      <section class="bg-image bg-image-4">
      </section>

      <section class="places">
        <h2>Places</h2>
        <p>As a matter of interest, I will try to list here the names of places Dad mentions, listed in alphabetical order.</p>
        <div class="container flex-box">
      
            <div>
              <ul>
                <li><a onclick="mapTo(this);">Abbeville</a></li>
                <li><a onclick="mapTo(this);">Aden</a></li>
                <li><a onclick="mapTo(this);">Ailly</a></li>
                <li><a onclick="mapTo(this);">Albany</a></li>
                <li><a onclick="mapTo(this);">Albert</a></li>
                <li><a onclick="mapTo(this);">Alexandria</a></li>
                <li><a onclick="mapTo(this);">Amiens</a></li>
                <li><a onclick="mapTo(this);">Anzac</a></li>
                <li><a onclick="mapTo(this);">Baileul</a></li>
                <li><a onclick="mapTo(this);">Bapaume</a></li>
                <li><a onclick="mapTo(this);">Beauval</a></li>
                <li><a onclick="mapTo(this);">Beauval</a></li>
                <li><a onclick="mapTo(this);">Bellancourt</a></li>
                <li><a onclick="mapTo(this);">Boescheppe</a></li>
                <li><a onclick="mapTo(this);">Bonneville</a></li>
                <li><a onclick="mapTo(this);">Buire</a></li>
                <li><a onclick="mapTo(this);">Cairo</a></li>
                <li><a onclick="mapTo(this);">Cassel</a></li>
                <li><a onclick="mapTo(this);">Colombo(Sri-Lanka)</a></li>
                <li><a onclick="mapTo(this);">Contalmaison</a></li>
                <li><a onclick="mapTo(this);">Croisy</a></li>
                <li><a onclick="mapTo(this);">Dardenelles</a></li>
              </ul>
            </div>
            <div>
              <ul>
                <li><a onclick="mapTo(this);">Dernancourt</a></li>
                <li><a onclick="mapTo(this);">Doullens</a></li>
                <li><a onclick="mapTo(this);">Egypt</a></li>
                <li><a onclick="mapTo(this);">Enoggera</a></li>
                <li><a onclick="mapTo(this);">Estaires</a></li>
                <li><a onclick="mapTo(this);">Fleurs</a></li>
                <li><a onclick="mapTo(this);">Fort Ricasoli</a></li>
                <li><a onclick="mapTo(this);">Fort Tigne</a></li>
                <li><a onclick="mapTo(this);">Fricourt</a></li>
                <li><a onclick="mapTo(this);">Gab-el-Habieta</a></li>
                <li><a onclick="mapTo(this);">Gallipoli</a></li>
                <li><a onclick="mapTo(this);">Godeswanvelde</a></li>
                <li><a onclick="mapTo(this);">Grantham</a></li>
                <li><a onclick="mapTo(this);">Halleborouck</a></li>
                <li><a onclick="mapTo(this);">Halloy-Pernois</a></li>
                <li><a onclick="mapTo(this);">Harponville</a></li>
                <li><a onclick="mapTo(this);">Havre</a></li>
                <li><a onclick="mapTo(this);">Hazebrouck</a></li>
                <li><a onclick="mapTo(this);">Hedauville</a></li>
                <li><a onclick="mapTo(this);">Hellabouk</a></li>
                <li><a onclick="mapTo(this);">Helmich</a></li>
                <li><a onclick="mapTo(this);">Henencourt Wood</a></li>
              </ul>
            </div>
            <div>
              <ul>
                <li><a onclick="mapTo(this);">Herrincort</a></li>
                <li><a onclick="mapTo(this);">Hill 60</a></li>
                <li><a onclick="mapTo(this);">Kasî-el-Nil Barracks</a></li>
                <li><a onclick="mapTo(this);">La Boiselle</a></li>
                <li><a onclick="mapTo(this);">Le Panne</a></li>
                <li><a onclick="mapTo(this);">Lemnos Is.</a></li>
                <li><a onclick="mapTo(this);">Liderzelle</a></li>
                <li><a onclick="mapTo(this);">London</a></li>
                <li><a onclick="mapTo(this);">Longueval</a></li>
                <li><a onclick="mapTo(this);">Malta</a></li>
                <li><a onclick="mapTo(this);">Marseilles</a></li>
                <li><a onclick="mapTo(this);">Melaulte</a></li>
                <li><a onclick="mapTo(this);">Melbourne</a></li>
                <li><a onclick="mapTo(this);">Mena Camp</a></li>
                <li><a onclick="mapTo(this);">Naours</a></li>
                <li><a onclick="mapTo(this);">Noordpiene</a></li>
                <li><a onclick="mapTo(this);">Palmas</a></li>
                <li><a onclick="mapTo(this);">Pinkenba</a></li>
                <li><a onclick="mapTo(this);">Polincove</a></li>
                <li><a onclick="mapTo(this);">Pont-Remy</a></li>
                <li><a onclick="mapTo(this);">Poperinghe</a></li>
                <li><a onclick="mapTo(this);">Port Said</a></li>
              </ul>
            </div>
            <div>
              <ul>
                <li><a onclick="mapTo(this);">Pozieres</a></li>
                <li><a onclick="mapTo(this);">Proven</a></li>
                <li><a onclick="mapTo(this);">Reninghelst</a></li>
                <li><a onclick="mapTo(this);">Rubempre</a></li>
                <li><a onclick="mapTo(this);">Sailley</a></li>
                <li><a onclick="mapTo(this);">Serapeum</a></li>
                <li><a onclick="mapTo(this);">Slienna</a></li>
                <li><a onclick="mapTo(this);">Somme</a></li>
                <li><a onclick="mapTo(this);">Southampton</a></li>
                <li><a onclick="mapTo(this);">Steenvoorde</a></li>
                <li><a onclick="mapTo(this);">Strazelle</a></li>
                <li><a onclick="mapTo(this);">Suez</a></li>
                <li><a onclick="mapTo(this);">Sutton</a></li>
                <li><a onclick="mapTo(this);">Tel-el-Kebir</a></li>
                <li><a onclick="mapTo(this);">Toutencourt</a></li>
                <li><a onclick="mapTo(this);">Trones Wood</a></li>
                <li><a onclick="mapTo(this);">Valetta</a></li>
                <li><a onclick="mapTo(this);">Varrenes</a></li>
                <li><a onclick="mapTo(this);">W.A.</a></li>
                <li><a onclick="mapTo(this);">Watten</a></li>
                <li><a onclick="mapTo(this);">Ypres</a></li>
                <li><a onclick="mapTo(this);">Zeitoun</a></li>
                </ul>
              </div>
      
        </div>
  
      </section>
      
    </main>

    <footer>
        <div class="flex-box">
          <div>
            <div>Site links</div> 
            <ul>
              <li><a onclick="slideTo('section.home');">Home</a></li>
              <li><a onclick="slideTo('section.letters');">Letters</a></li>
              <li><a onclick="slideTo('section.postcards');">Postcards</a></li>
              <li><a onclick="slideTo('section.places');">Places</a></li>
            </ul>
          </div>
          <div class="photo"></div>
          <div>
            <p>At this time, comments <br/>should be emailed to;</p>
            <p><a href="mailto:ibak6837@bigpond.net.au">ibak6837@bigpond.net.au</a></p>
            <p>Thank you.</p>
          </div>
        </div> 

        <div>&copy;<script>
          document.write(new Date().getFullYear());
        </script> Shaun Campbell. Last modified <?= date("Y F d  H:i", filemtime($_SERVER['SCRIPT_FILENAME'])); ?>.</div>
        <div>Disclaimer: This website is not a real website and is being developed as part of a School of Science Web Programming course at RMIT University in Melbourne, Australia.</div>
        <div><button id='toggleWireframeCSS' onclick='toggleWireframe()'>Toggle Wireframe CSS</button></div>
      </div>
    </footer>

  </body>

  <script>
    var loadContent = function (sectionSelector, contentSelector) {
      var allContent = document.querySelectorAll(sectionSelector + ' .content > div');
      allContent.forEach(function (val) {
        val.classList.remove("visible-content");        
      });
      var specificContent = document.querySelector(contentSelector);
      specificContent.classList.add('visible-content');
    }

    var slideTo = function (selector) {
      var element = document.querySelector(selector);
      if (!element) {
        console.log('scrollTo(): selector ' + selector + ' not found');
        return;
      }
      var top = element.getBoundingClientRect().top;
      window.scrollTo({ top: (top  + window.scrollY), behavior: 'smooth' });
    }

    var mapTo = function (element) {
      var url = 'https://www.google.com/maps/search/?api=1&query=' + element.innerText;
      window.open(url,  '_blank');
    }
  </script>
</html>