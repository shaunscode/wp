<?php

    $inputs = ['name', 'email', 'mobile', 'subject', 'message'];
    $patterns = array(
        'name' => '/(?<FirstName>[A-Z]\.?\w*\-?[A-Z]?\w*)\s?(?<MiddleName>[A-Z]\w*|[A-Z]?\.?)\s?(?<LastName>[A-Z]\w*\-?[A-Z]?\w*)(?:,\s|)(?<Suffix>Jr\.|Sr\.|IV|III|II|)/', 
        'email' => '/[\w-]+@([\w-]+\.)+[\w-]+/',
        'mobile' => '/04[0-9]{8}/',
        'subject' => '/^([1-zA-Z0-1@.\s]{1,255})$/',
    );
    $hints = array(
        'name' => 'Did you forget to capitalise your name? eg John Smith',
        'email' => 'Remember to use appropriate formatting, xxx @ yyyy . zzz etc',
        'mobile' => 'We only accept Australian mobiles numbers, did yours start with 04?',
        'subject' => 'Remember to add a kind message! (255 chars only)'
    );

    function getParamValues ($inputs) {
        $values = [];
        forEach($inputs as $input) {
            $values[$input] = isset($_POST[$input])? $_POST[$input] : null;
        }
        return $values;
    }

    function testValidations ($inputs, $patterns) {
        $failures = [];
        forEach($inputs as $key => $value) {
            //special handing for mobile
            if (!($key === 'mobile' && strlen($inputs[$key]) === 0)) {
                if (array_key_exists($key, $patterns) && preg_match($patterns[$key], $value) === 0 ) { //failed
                    $failures[$key] = $value;
                }
            }
        }
        return $failures;
    }

    function getSantisedValues ($values) {
        $sanatised = [];
        forEach($values as $key => $val) {
            $sanatised[$key] = htmlspecialchars($val);
        }
        return $sanatised;
    }
    
    function getSuccessResponse () {
        return '{
            "apiResponse": "success",
            "apiMessages": ["Successfully submitted contact values"]
        }';
    }

    function getFailuresResponse ($fails, $hints) {
        $res = '{
            "apiResponse": "failure",
            "apiMessages": ["One of more service side validation failed.",';
        
        $index = 0;
        forEach($fails as $key => $val) {
            $hint =  isset($hints[$key])? $hints[$key] : null;
            $res .= '"' . $key . ': ' . $val . ' ' . ($hint ? '(hint: ' . $hint . ')' : '') . '"';
            if ($index !== count($fails) - 1) {
                $res .= ', ';
            }
            $index++;
        }                
        $res .= ']}';
        return $res;
    }

    function storeInputs ($inputs) {
        $file = fopen("mail.txt","a");
        fputcsv($file, array_values($inputs));
        fclose($file);
    }

    function getFailedToSaveResponse ($err) {
        return '{
            "apiResponse": "failure",
            "apiMessages": ["Failed to save details", $err]
        }';

    }

    $values = getParamValues($inputs);
    $failures = testValidations($values, $patterns);
    if ($failures != null & count($failures) > 0) {
        echo getFailuresResponse($failures, $hints);
        return;
    }
    $sanatised = getSantisedValues($values);
    try {
        storeInputs($sanatised);
        echo getSuccessResponse();
    } catch (Exception $e) {
       echo getFailedToSaveRespons($e->getMessage());
    }
    
?>