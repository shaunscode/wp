<?php
  session_start();

// Put your PHP functions and modules here


$data;
function drawLetters () {
  global $data;
  if (is_null($data)) {
    $data = loadData();
  }
  foreach ($data as $entryData) {
    if ($entryData["Type"] == "Letter") {
      drawLetter($entryData);
    }
  }
}

function drawLetter ($letterData) {
  echo '
      <div class="letter" onclick="turn(this);" data-isshowingfront="true" data-isanimating="false" data-type="letter">                
      <div class="letter-inner">
        <div class="letter-front">
          <span>' . date_format(date_create($letterData['DateStart']), "l jS\nF Y " ) . '</span>
        </div>
        <div class="letter-back">
          <article> ' . str_replace("\n", "<br/>", $letterData['Content']) . '</article>
        </div>
      </div>
    </div>
  ';
}

function drawPostcards () {
  global $data;
  if (is_null($data)) {
    $data = loadData();
  }
  $index = 1;
  foreach ($data as $entryData) {
    if ($entryData["Type"] == "Postcard") {
      drawPostcard($entryData, $index);
      $index++;
    }
  }
}

function drawPostcard ($postcardData, $index) {
  echo '
    <div class="postcard" onclick="turn(this);" data-isshowingfront="true" data-isanimating="false" data-type="postcard">                
      <div class="postcard-inner">
        <div class="postcard-front" style="background-image: url(\'../../media/postcard' . ($index % 4 + 1)  . '.jpg\')">
          </div>
        <div class="postcard-back">
          <span>' . date_format(date_create($postcardData['DateStart']), "l jS\nF Y " ) . '</span>
          <article> ' . str_replace("\n", "<br/>", $postcardData['Content']) . '</article>
        </div>
      </div>
    </div>';   
  }

  function loadData () {
    
    $file = fopen("http://titan.csit.rmit.edu.au/~e54061/wp/letters-home.txt","r");
    if (is_null($file)) {
      return [];
    }
    $headings = fgetcsv($file, 0, "\t");
    $entries = [];
    while(! feof($file)) {
      $entryData = fgetcsv($file, 0, "\t");
      $entry = [];
      $index = 0;
      foreach ($entryData as $data) {
        $entry[$headings[$index]] = $data;  
        $index++;
      }
      array_push($entries, $entry);
    } 
    
    fclose($file);
    return $entries;
  }

?>