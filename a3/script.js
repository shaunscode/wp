//https://www.w3schools.com/xml/ajax_xmlhttprequest_response.asp

const slideTo = function(selector) {
    var element = document.querySelector(selector);
    if (!element) {
        console.log('scrollTo(): selector ' + selector + ' not found');
        return;
    }
    var top = element.getBoundingClientRect().top;
    window.scrollTo({
        top: (top + window.scrollY),
        behavior: 'smooth'
    });
}

const mapTo = function(element) {
    var url = 'https://www.google.com/maps/search/?api=1&query=' + element.innerText;
    window.open(url, '_blank');
}

/* Insert your javascript here */
const dump = function(ele) {
    console.log('dump: ', ele);
}

const Constants = {
    animDuration: 1,
    letterHeight: `60vh`,
    letterWidth: `${60 / 4 * 8}vh`,
    postcardHeight: `65vh`,
    postcardWidth: `100vh`,
    entryContMargin: `14vw`,
    entryMargin: '5vh',
    type: {
        letter: 'letter',
        postcard: 'postcard'
    },
    inputIds: ['name', 'email', 'mobile', 'subject', 'message'],
    rememberMeId: 'ANZAC-DoublasRaymondBaker-Storage',
    msgType: {
        success: 'Success',
        failure: 'Failure',
        warning: 'Warning',
        info: 'Info'
    }
}

const getAll = function(selector) {
    const eles = document.querySelectorAll(selector);
    if (!eles) {
        //console.log(`### ERROR: No elements found for selector '${selector}'`);
        return undefined;
    }
    //console.log(`+++ Found ${eles.length} elements for selector '${selector}'`)
    return eles;
}

const get = function(selector) {
    const ele = document.querySelector(selector);
    if (!ele) {
        //console.log(`### ERROR: No element found for selector '${selector}'`);
        return undefined;
    }
    //console.log(`+++ Found element for selector '${selector}'`)
    return ele;
}

const drawLetters = function() {
    const lettersCont = get('.letters-container');
    const letters = getAll('.letter');

    if (!lettersCont || !letters) {
        return;
    }

    // lettersCont.style.height = `calc(${letters.length} * ${Constants.letterHeight})`

    letters.forEach(function(letter, i) {
        letter.style.width = Constants.letterWidth;
        letter.style.height = Constants.letterHeight;
        // letter.style.left = `calc((100vw - ${Constants.letterWidth}) / 2)`
        letter.style.transition = `${Constants.animDuration}s transform`;
        letter.children[0].style.transition = `${Constants.animDuration}s transform`;
        letter.style.zIndex = `${i}`;
        letter.style.top = `${Constants.entryMargin}`;

        letter.children[0].style.transform = `rotate(parseInt(${(Math.random() * 10)- 10}deg)`;
    });
}

const drawPostcards = function() {
    const postcardCont = get('.postcard-container');
    const postcards = getAll('.postcard');

    if (!postcardCont || !postcards) {
        return;
    }

    //  postcardCont.style.height = `calc(${postcards.length} * ${Constants.postcardHeight})`

    postcards.forEach(function(postcard, i) {
        postcard.style.width = Constants.postcardWidth;
        postcard.style.height = Constants.postcardHeight;
        // letter.style.left = `calc((100vw - ${Constants.letterWidth}) / 2)`
        postcard.style.transition = `${Constants.animDuration}s transform`;
        postcard.children[0].style.transition = `${Constants.animDuration}s transform`;
        postcard.style.zIndex = `${i}`;
        postcard.style.top = `${Constants.entryMargin}`;

        postcard.children[0].style.transform = `rotate(parseInt(${(Math.random() * 10)- 10}deg)`;
    });
}

const getElements = function(ele) {
    return {
        type: ele.dataset.type,
        outter: ele,
        inner: ele.children[0],
        front: ele.children[0].children[0],
        back: ele.children[0].children[1]
    }
}

const turn = function(ele) {
    if (ele.dataset.isanimating === 'true') {
        return;
    }

    const eles = getElements(ele);
    turnSided(eles);
    ele.dataset.isanimating = 'true';
    setTimeout(function() {
        turnFaced(eles);

        if (ele.dataset.isshowingfront === 'true') {
            showBack(eles);
            ele.dataset.isshowingfront = 'false'
        } else {
            showFront(eles);
            ele.dataset.isshowingfront = 'true';
        }
        setTimeout(function() {
            ele.dataset.isanimating = 'false'
        }, Constants.animDuration * 1000);

    }, Constants.animDuration * 1000); //millis
}

const showFront = function(entry) {
    entry.outter.style.width = Constants[`${entry.type}Width`];
    entry.outter.style.height = Constants[`${entry.type}Height`];
    entry.front.style.display = 'block';
    entry.back.style.display = 'none';
};

const showBack = function(entry) {
    entry.outter.style.width = Constants[`${entry.type}Width`];
    entry.outter.style.height = Constants[`${entry.type}Height`];
    entry.front.style.display = 'none';
    entry.back.style.display = 'block';
};

const turnFaced = function(entry) {
    entry.outter.style.transform = 'scale(1, 1)';
    // entry.inner.style.transform = 'rotate(0deg)';
}

const turnSided = function(entry) {
    entry.outter.style.transform = 'scale(0, 1.5)';
    //entry.inner.style.transform = 'rotate(10deg)';

}

const rememberMe = function() {
    const inputs = getInputs();
    if (inputs.remember.ele.checked) {
        localStorage.setItem(Constants.rememberMeId, JSON.stringify({
            name: inputs.name.ele.value,
            email: inputs.email.ele.value,
            mobile: inputs.mobile.ele.value ? inputs.mobile.ele.value : ''
        }));
    } else {
        localStorage.removeItem(Constants.rememberMeId);
    }
}

const initRememberMe = function() {
    const storedContact = localStorage.getItem(Constants.rememberMeId);
    if (storedContact) {
        let contact;
        try {
            contact = JSON.parse(storedContact);
        } catch (err) {
            addMessage(Constants.msgType.error, 'Failed to parse stored contact: ' + err);
        }
        const inputs = getInputs();
        inputs.name.ele.value = contact.name;
        inputs.email.ele.value = contact.email;
        inputs.mobile.ele.value = contact.mobile ? contact.mobile : '';
        inputs.remember.ele.checked = true;
        addMessage(Constants.msgType.info, (`Welcome back  ${contact.name} (${contact.email})!`));
    }
}


//name: Author David Walker, published unknown, Regular Expression Library, https://regexlib.com/Search.aspx?k=name&c=-1&m=-1&ps=20, Page 1, viewed 18/08/2020
//email: Author Steven Smith, published unknown, Regular Expression Library, https://regexlib.com/Search.aspx?k=email&c=-1&m=-1&ps=20, Page 1, viewed 18/08/2020
//subject: Author Paul Miller, published unknown, Regular Expression Library, https://regexlib.com/Search.aspx?k=name&c=-1&m=-1&ps=20, Page 1, viewed 18/08/2020
const getInputs = function() {
    return {
        name: {
            ele: get('input#name'),
            pattern: /(?<FirstName>[A-Z]\.?\w*\-?[A-Z]?\w*)\s?(?<MiddleName>[A-Z]\w*|[A-Z]?\.?)\s?(?<LastName>[A-Z]\w*\-?[A-Z]?\w*)(?:,\s|)(?<Suffix>Jr\.|Sr\.|IV|III|II|)/,
            failedMsg: 'Please provide and appropriate capitalised name.'
        },
        email: {
            ele: get('input#email'),
            pattern: /[\w-]+@([\w-]+\.)+[\w-]+/,
            failedMsg: 'Please provide and appropriate email (abc@def.com)'
        },
        mobile: {
            ele: get('input#mobile'),
            pattern: /04[0-9]{8}/,
            failedMsg: 'Please provide and appropriate Australia phone number'
        },
        subject: {
            ele: get('input#subject')
        },
        message: {
            ele: get('textarea#message')
        },
        remember: {
            ele: get('input#remember')
        }
    };
}

const validateInput = function(ele) {
    const err = get(`#${ele.id} + div.validation`);
    const input = getInputs()[ele.id];
    input.ele.classList.remove('invalid');

    if (input.ele.required) {
        if (input.ele.value.length === 0) {
            input.ele.classList.add('invalid');
            err.innerHTML = "Field required";
            return;
        }
    }
    if (input.pattern) {
        if (input.ele.value.length > 0) {
            if (!input.pattern.test(input.ele.value)) {
                input.ele.classList.add('invalid');
                err.innerHTML = input.failedMsg;
            }
        }
    }
}

const validateInputs = function() {
    const inputs = getInputs();
    Object.values(inputs).forEach(input => {
        validateInput(input.ele);
    });
}

const clearInputs = function() {
    const inputs = getInputs();
    Object.keys(inputs).forEach(key => inputs[key].ele.value = '');

}

const getContactValues = function() {
    const inputs = getInputs();
    const values = new FormData();
    Object.keys(inputs).forEach(key => values.append(key, inputs[key].ele.value));
    return values;
}

const submitContact = function() {
    validateInputs();
    const failures = getAll('.invalid');
    if (failures && failures.length > 0) {
        return;
    }
    const values = getContactValues();
    callApi('POST', './api.php', function(xhttp) {
        addApiMessage(xhttp.responseText);
        clearInputs();

    }, values)
}

const addListeners = function() {
    Object.values(getInputs()).map(val => val.ele).forEach(ele =>
        ele.addEventListener('blur', function() {
            validateInput(ele);
        })
    )
}

const callApi = function(method, url, callback, data) {
    var xhttp = new XMLHttpRequest();
    xhttp.open(method, url, true);
    //xhttp.setRequestHeader("Content-Type", "multipart/form-data");
    xhttp.onreadystatechange = function() {
        //console.log(`callPostApi(): readyState: ${this.readyState}, status: ${this.status}`);
        if (this.readyState == 4 && this.status == 200) {
            //console.log('calllPostApi(): ready, calling callback method with: ', this);
            callback(this);
        }
    };
    xhttp.send(data);
}

const removeMessage = function(ele) {
    const msgNode = ele.parentNode;
    const msgContainer = msgNode.parentNode;
    msgContainer.classList.add('opaque');
    setTimeout(() => msgContainer.childNodes.forEach(child => msgContainer.remove(child)), 1000);

}

const addMessage = function(type, message, details) {
    const ele = get('div.siteMessages');
    let msgs = ele.innerHTML;
    msgs = `${msgs}<div class="siteMessage opaque siteMessage${type}"><div>${type}: ${message}<span onclick="removeMessage(this)">(close)</span></div>`
    if (details && details.length > 0) {
        msgs += `<div><ul>`;
        details.forEach(detail => msgs += `<li>${detail}</li>`);
        msgs += `</ul></div>`
    }
    msgs += `</div>`;
    ele.innerHTML = msgs;
    setTimeout(() => {
        const msg = get('.siteMessage.opaque').classList.remove('opaque');
    }, 1);
}

const addApiMessage = function(res) {
    try {
        const resObj = JSON.parse(res);
        const msgType = Constants.msgType[resObj.apiResponse];
        const msg = resObj.apiMessages[0];
        if (resObj.apiMessages.length > 0) {
            resObj.apiMessages.shift();
        }
        addMessage(msgType, msg, resObj.apiMessages);
    } catch (err) {
        addMessage(Constants.msgType.failure, err, [res]);
    }
}


//execute
const docReady = function(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

docReady(function() {
    drawLetters();
    drawPostcards();
    initRememberMe();
    addListeners();
});