<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ANZAC DouglasR Raymond Baker: Letters Home</title>
    
    <!-- Keep wireframe.css for debugging, add your css to style.css -->
    <link id='wireframecss' type="text/css" rel="stylesheet" href="../wireframe.css" disabled>
    <link id='stylecss' type="text/css" rel="stylesheet" href="style.css?t=<?= filemtime("style.css"); ?>">

    <link href="https://fonts.googleapis.com/css2?family=Give+You+Glory&family=Sora:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Dawning+of+a+New+Day&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Gloria+Hallelujah&display=swap" rel="stylesheet">
    
    <script src='../wireframe.js'></script>
    <script src='./script.js'></script>
  </head>

  <body>
  
  <?php include 'tools.php'; ?>


  <div class="siteMessages"></div>

    <header>
      <div>ANZAC</div>
      <div>Douglas Raymond Baker</div>
      <h1>Letters <br/>Home</h1></div>
    </header>

    <nav>
      <div>
      <ul>
        <li><a onclick="slideTo('section.home');">Home</a></li>
        <li><a onclick="slideTo('section.letters');">Letters</a></li>
        <li><a onclick="slideTo('section.postcards');">Postcards</a></li>
        <li><a onclick="slideTo('section.contact');">Contact</a></li>
        <li><a onclick="slideTo('section.places');">Places</a></li>
      </ul>
</div>
    </nav>

    <main>
      
      <section class="bg-image bg-image-1">
      </section>

      <section class="home">
        <h2>Home</h2>
        <article class="container">
          <p>Hello and welcome,</p>
          <p>This year is the centenary of the birth of the ANZAC legend. As such, many people, particularly young people, are looking for ways to connect with people of that period and inparticular, those who created the ANZAC legend.</p>
          <p>This site presents the letters of Douglas Raymond Baker, who came from Gympie, Queensland, Australia. He enlisted in August 1914 and during the years that followed, he wrote letters and post cards to his family at home. In them, he describes much of the goings-on of the life of an ANZAC, his feeling and opinions, and experiences while visiting his relatives in England during leave.</p>
          <p>They start from the beginning of basic training in Brisbane in August 1914 and end in May 1918.</p>
          <p>They are offered here so that others may get an understanding of life as an ANZAC and get a sense of what life on the battlefield was like.</p>
          <p>From the menu on the left, read the Introduction to set the scene. Then, to start reading the letters, click on Letter and Post Cards in the menu on the left. All the letters are searchable using the  search bar at the top right of this page.</p>
        </article>
      </section>

      <section class="bg-image bg-image-2">
      </section>

      <section class="letters">
        <h2>Letters</h2>            
          <div class="content">         
            <div class="letters-container">
              <?php drawLetters(); ?>
            </div>
          </div>
      </section>
      
      <section class="bg-image bg-image-3">
      </section>

      <section class="postcards">
        <h2>Postcards</h2>            
          <div class="content">   
            <div class="postcard-container">
              <?php drawPostcards(); ?>
          </div>
        </div>

      </section>

      <section class="bg-image bg-image-4">
      </section>

      <section class="contact">
        <h2>Contact</h2>            
        <div class="content">   
          
        <form class="contactForm" >
          <fieldset>
            <legend>Contact us</legend>
            <div>Labels with <span class="required-field">*</span> are required fields</div>

            <div class="contact-grid">
              <label for="name">Name <span class="required-field">*</span></label>
              <div>
                <input type="text" id="name" name="name" required placeholder="Firstname Lastname (capitalized)" >
                <div class="validation"></div>
              </div>
              
              <label for="email">Email <span class="required-field">*</span></label>
              <div>
                <input type="email" id="email" name="email"  required placeholder="abc@def.com">
                <div class="validation"></div>
              </div>
              
              <label for="mobile">Mobile </label>
              <div>
                <input type="text" id="mobile" name="mobile" placeholder="0400123456" />
                <div class="validation"></div>
              </div>
              
              <label for="subject">Subject <span class="required-field">*</span></label>
              <div>
                <input type="text" id="subject" name="subject"  required placeholder="Subject" />
                <div class="validation"></div>
              </div>
              
              <label for="message">Message <span class="required-field">*</span></label> 
              <div>
                <textarea id="message" name="message"  maxlength="1047" required placeholder="Please type message here" rows="5"></textarea> 
                <div class="validation"></div>
              </div>
              
              <span></span>
              <div>
                <input type="checkbox" id="remember" name="remember" onclick="rememberMe(this);"/>
                <label for="remember">Remember me</label>
              </div>
            
            </div>
          </fieldset>
          
          <input type="button" value="Send" onclick="submitContact()">
          </form>
        </div>
      </section>

      <section class="bg-image bg-image-5">
      </section>

      <section class="places">
        <h2>Places</h2>
        <p>As a matter of interest, I will try to list here the names of places Dad mentions, listed in alphabetical order.</p>
        <div class="container flex-box">
      
            <div>
              <ul>
                <li><a onclick="mapTo(this);">Abbeville</a></li>
                <li><a onclick="mapTo(this);">Aden</a></li>
                <li><a onclick="mapTo(this);">Ailly</a></li>
                <li><a onclick="mapTo(this);">Albany</a></li>
                <li><a onclick="mapTo(this);">Albert</a></li>
                <li><a onclick="mapTo(this);">Alexandria</a></li>
                <li><a onclick="mapTo(this);">Amiens</a></li>
                <li><a onclick="mapTo(this);">Anzac</a></li>
                <li><a onclick="mapTo(this);">Baileul</a></li>
                <li><a onclick="mapTo(this);">Bapaume</a></li>
                <li><a onclick="mapTo(this);">Beauval</a></li>
                <li><a onclick="mapTo(this);">Beauval</a></li>
                <li><a onclick="mapTo(this);">Bellancourt</a></li>
                <li><a onclick="mapTo(this);">Boescheppe</a></li>
                <li><a onclick="mapTo(this);">Bonneville</a></li>
                <li><a onclick="mapTo(this);">Buire</a></li>
                <li><a onclick="mapTo(this);">Cairo</a></li>
                <li><a onclick="mapTo(this);">Cassel</a></li>
                <li><a onclick="mapTo(this);">Colombo(Sri-Lanka)</a></li>
                <li><a onclick="mapTo(this);">Contalmaison</a></li>
                <li><a onclick="mapTo(this);">Croisy</a></li>
                <li><a onclick="mapTo(this);">Dardenelles</a></li>
              </ul>
            </div>
            <div>
              <ul>
                <li><a onclick="mapTo(this);">Dernancourt</a></li>
                <li><a onclick="mapTo(this);">Doullens</a></li>
                <li><a onclick="mapTo(this);">Egypt</a></li>
                <li><a onclick="mapTo(this);">Enoggera</a></li>
                <li><a onclick="mapTo(this);">Estaires</a></li>
                <li><a onclick="mapTo(this);">Fleurs</a></li>
                <li><a onclick="mapTo(this);">Fort Ricasoli</a></li>
                <li><a onclick="mapTo(this);">Fort Tigne</a></li>
                <li><a onclick="mapTo(this);">Fricourt</a></li>
                <li><a onclick="mapTo(this);">Gab-el-Habieta</a></li>
                <li><a onclick="mapTo(this);">Gallipoli</a></li>
                <li><a onclick="mapTo(this);">Godeswanvelde</a></li>
                <li><a onclick="mapTo(this);">Grantham</a></li>
                <li><a onclick="mapTo(this);">Halleborouck</a></li>
                <li><a onclick="mapTo(this);">Halloy-Pernois</a></li>
                <li><a onclick="mapTo(this);">Harponville</a></li>
                <li><a onclick="mapTo(this);">Havre</a></li>
                <li><a onclick="mapTo(this);">Hazebrouck</a></li>
                <li><a onclick="mapTo(this);">Hedauville</a></li>
                <li><a onclick="mapTo(this);">Hellabouk</a></li>
                <li><a onclick="mapTo(this);">Helmich</a></li>
                <li><a onclick="mapTo(this);">Henencourt Wood</a></li>
              </ul>
            </div>
            <div>
              <ul>
                <li><a onclick="mapTo(this);">Herrincort</a></li>
                <li><a onclick="mapTo(this);">Hill 60</a></li>
                <li><a onclick="mapTo(this);">Kasî-el-Nil Barracks</a></li>
                <li><a onclick="mapTo(this);">La Boiselle</a></li>
                <li><a onclick="mapTo(this);">Le Panne</a></li>
                <li><a onclick="mapTo(this);">Lemnos Is.</a></li>
                <li><a onclick="mapTo(this);">Liderzelle</a></li>
                <li><a onclick="mapTo(this);">London</a></li>
                <li><a onclick="mapTo(this);">Longueval</a></li>
                <li><a onclick="mapTo(this);">Malta</a></li>
                <li><a onclick="mapTo(this);">Marseilles</a></li>
                <li><a onclick="mapTo(this);">Melaulte</a></li>
                <li><a onclick="mapTo(this);">Melbourne</a></li>
                <li><a onclick="mapTo(this);">Mena Camp</a></li>
                <li><a onclick="mapTo(this);">Naours</a></li>
                <li><a onclick="mapTo(this);">Noordpiene</a></li>
                <li><a onclick="mapTo(this);">Palmas</a></li>
                <li><a onclick="mapTo(this);">Pinkenba</a></li>
                <li><a onclick="mapTo(this);">Polincove</a></li>
                <li><a onclick="mapTo(this);">Pont-Remy</a></li>
                <li><a onclick="mapTo(this);">Poperinghe</a></li>
                <li><a onclick="mapTo(this);">Port Said</a></li>
              </ul>
            </div>
            <div>
              <ul>
                <li><a onclick="mapTo(this);">Pozieres</a></li>
                <li><a onclick="mapTo(this);">Proven</a></li>
                <li><a onclick="mapTo(this);">Reninghelst</a></li>
                <li><a onclick="mapTo(this);">Rubempre</a></li>
                <li><a onclick="mapTo(this);">Sailley</a></li>
                <li><a onclick="mapTo(this);">Serapeum</a></li>
                <li><a onclick="mapTo(this);">Slienna</a></li>
                <li><a onclick="mapTo(this);">Somme</a></li>
                <li><a onclick="mapTo(this);">Southampton</a></li>
                <li><a onclick="mapTo(this);">Steenvoorde</a></li>
                <li><a onclick="mapTo(this);">Strazelle</a></li>
                <li><a onclick="mapTo(this);">Suez</a></li>
                <li><a onclick="mapTo(this);">Sutton</a></li>
                <li><a onclick="mapTo(this);">Tel-el-Kebir</a></li>
                <li><a onclick="mapTo(this);">Toutencourt</a></li>
                <li><a onclick="mapTo(this);">Trones Wood</a></li>
                <li><a onclick="mapTo(this);">Valetta</a></li>
                <li><a onclick="mapTo(this);">Varrenes</a></li>
                <li><a onclick="mapTo(this);">W.A.</a></li>
                <li><a onclick="mapTo(this);">Watten</a></li>
                <li><a onclick="mapTo(this);">Ypres</a></li>
                <li><a onclick="mapTo(this);">Zeitoun</a></li>
              </ul>
            </div>
      
        </div>
  
      </section>
      
    </main>

    <footer>
        <div class="flex-box">
          <div>
            <div>Site links</div> 
            <ul>
              <li><a onclick="slideTo('section.home');">Home</a></li>
              <li><a onclick="slideTo('section.letters');">Letters</a></li>
              <li><a onclick="slideTo('section.postcards');">Postcards</a></li>
              <li><a onclick="slideTo('section.contact');">Contact</a></li>
              <li><a onclick="slideTo('section.places');">Places</a></li>
            </ul>
          </div>
          <div class="photo"></div>
        </div> 

        <div>&copy;<script>
          document.write(new Date().getFullYear());
        </script> Shaun Campbell. Last modified <?= date("Y F d  H:i", filemtime($_SERVER['SCRIPT_FILENAME'])); ?>.</div>
        <div>Disclaimer: This website is not a real website and is being developed as part of a School of Science Web Programming course at RMIT University in Melbourne, Australia.</div>
        <div><button id='toggleWireframeCSS' onclick='toggleWireframe()'>Toggle Wireframe CSS</button></div>
      </div>
    </footer>


  </body>
 
</html>